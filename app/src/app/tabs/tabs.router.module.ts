import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: '../tab1/tab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'pagina1',
        children: [
          {
            path: '',
            loadChildren: '../pagina1/pagina1.module#Pagina1PageModule'
          }
        ]
      },
      {
        path: 'pagina2',
        children: [
          {
            path: '',
            loadChildren: '../pagina2/pagina2.module#Pagina2PageModule'
          }
        ]
      },
      {
        path: 'pagina3',
        children: [
          {
            path: '',
            loadChildren: '../pagina3/pagina3.module#Pagina3PageModule'
          }
        ]
      },
      {
        path: 'pagina4',
        children: [
          {
            path: '',
            loadChildren: '../pagina4/pagina4.module#Pagina4PageModule'
          }
        ]
      },

      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
